package com.example.colaboradores.services;

import com.example.colaboradores.models.Colaborador;

import java.util.List;

public interface ColaboradorService {

    List<Colaborador> findAll();
    public Colaborador findOne(String id);
    public Colaborador saveColaborador(Colaborador soc);
    public void updateColaborador(Colaborador soc);
    public void deleteColaborador(String id);
}
