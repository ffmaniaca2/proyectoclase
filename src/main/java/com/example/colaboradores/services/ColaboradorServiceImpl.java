package com.example.colaboradores.services;

import com.example.colaboradores.models.Colaborador;
import com.example.colaboradores.repository.ColaboradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("vehiculoService")
@Transactional
public class ColaboradorServiceImpl implements ColaboradorService {

        private ColaboradorRepository colaboradorRepository;

        @Autowired
        public ColaboradorServiceImpl(ColaboradorRepository colaboradorRepository)
        {
            this.colaboradorRepository = colaboradorRepository;
        }

        @Override
        public List<Colaborador> findAll() {
            return colaboradorRepository.findAll();
        }

        @Override
        public Colaborador findOne(String id) {
            return colaboradorRepository.findOne(id);
        }

        @Override
        public Colaborador saveColaborador(Colaborador soc) {
            return colaboradorRepository.saveColaborador(soc);
        }

        @Override
        public void updateColaborador(Colaborador soc) {
            colaboradorRepository.updateColaborador(soc);
        }

        @Override
        public void deleteColaborador(String id) {
            colaboradorRepository.deleteColaborador(id);
        }

}
