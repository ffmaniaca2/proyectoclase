package com.example.colaboradores.controllers;

import com.example.colaboradores.models.Colaborador;
import com.example.colaboradores.services.ColaboradorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("colaborador")
public class ColaboradorController {
    private final ColaboradorService ColaboradorService;

    @Autowired
    public ColaboradorController(ColaboradorService ColaboradorService) {
        this.ColaboradorService = ColaboradorService;
    }

    @GetMapping()
    public ResponseEntity<List<Colaborador>> Colaborador() {
        System.out.println("Me piden la lista de Colaborador");
        return ResponseEntity.ok(ColaboradorService.findAll());
    }

    @PostMapping()
    public ResponseEntity<Colaborador> saveVehiculo(@RequestBody Colaborador colaborador)
    {
        return ResponseEntity.ok(ColaboradorService.saveColaborador(colaborador));
    }
}
