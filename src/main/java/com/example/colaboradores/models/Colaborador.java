package com.example.colaboradores.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "colaboradores")
@JsonPropertyOrder({"registro", "nombre", "password"})
public class Colaborador implements Serializable {
    private String registro;
    private String nombre;
    private String password;


    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String idSocio) {
        this.registro = idSocio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
